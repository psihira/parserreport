<?php



/**
 * Класс для работы с ссылками
 *
 * Преобразует относительную ссылку в абсолютную при необходимости
 *
 */

class Links
{

    /**
     * Анализ ссылки и преобразование
     *
     * @param $base
     * @param $url
     * @return bool|mixed|string
     */
    public function resolveUrl($base, $url)
    {

        $base = trim($base, '/') . '/';
        $array_base = parse_url($base);
        $array_url = parse_url($url);
        $result = '';


        if(isset($array_url['scheme']) && isset($array_url['host'])){
            if (strpos($url,'.pdf') != false)
                $url = substr($url,0,strpos($url,'.pdf') + 4);
            return $url;
        }

        if(empty($array_base['host'])){
            return false;
        }
        $result .= $array_base['host'];

        if (!preg_match('~^(http://[^/?#]+)?([^?#]*)?(\?[^#]*)?(#.*)?$~i', $url.'#', $parseUrl)) {
            return false;
        }

        if(isset($array_url['path'])){
            $result .= $this->_normalise($parseUrl[2],$array_base['path']). $parseUrl[3];
        }

        if(isset($array_url['query'])){
            $result .= '?' . $array_url['query'];
        }

        $result = preg_replace("/\/{2,}/","/",$result);

        if(empty($array_base['scheme'])){
            $array_base['scheme'] = 'http';
        }
        $result = $array_base['scheme'] . '://' . $result;
        return $result;
    }

    /**
     *
     * Нормализация пути
     *
     * @param $link
     * @param $base
     * @return string
     */
    private function _normalise($link, $base)
    {

        $pathLink = explode('/', trim($link,'/'));
        $pathBase = explode('/',  $base);

        if (sizeOf($pathBase) > 0) {
            array_pop($pathBase);
        }
        foreach ($pathLink as $p) {
            if ($p == '.') {
                continue;
            } elseif ($p == '..') {
                if (sizeOf($pathBase) > 0) {
                    array_pop($pathBase);
                }
            } else {
                array_push($pathBase, $p);
            }
        }

        return implode('/', $pathBase);

    }

}


