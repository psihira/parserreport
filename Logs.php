<?php

require_once ('dbCore.php');


/**
 * Логирование ошибок и состояния обработки вуза
 *
 */

class Logs
{

    function __construct() {}

    /**
     * Запись ошибок в файл
     *
     * @param $data
     */
    public function createLog($data){
        $file = "reports/error_log.txt";
        $fh = fopen($file, 'a');
        fwrite($fh,$data."\n");
        fclose($fh);
    }


    /**
     * Запись статуса обработки вуза в БД
     *
     * @param $vuz
     * @param $kod
     * @param $file
     * @param $status
     */
    public function logReport($vuz, $kod, $file, $status){

        $db = dbCore::getInstance();
        $connection = $db->getConnection();
        $sql = "insert into `report` (vuz_id,file_name,kod,status)"
             . "values ($vuz,'$file',$kod,'$status')";
        $connection->query($sql);

    }

}