<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 23.08.2017
 * Time: 12:20
 */


require_once('dbCore.php');
require_once('parserSite.php');

class QueryDB
{

    private $_startPosition;

    /**
     * @param mixed $count
     */
    public function setStartPosition($startPosition)
    {
        $this->_startPosition = $startPosition;
    }


    /**
     *Получение данных из табл и их передача на обработку
     */
    public function startWork(){

        $db = dbCore::getInstance();
        $sql = $db->getConnection();
        $parse = new ParserSite();
        $query = "select kod, site, msd_id from vm where site is not null  order by msd_id asc limit {$this->_startPosition}, 200";
        foreach ($sql->query($query) as $row){
            $parse->setVuzID($row['msd_id']);
            $parse->setKod($row['kod']);
            $parse->setUrlSite($row['site']);
            $parse->startParseSite();
        }

    }


}