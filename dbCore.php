<?php



require_once 'config.php';

class dbCore
{
    private $_connection;
    private static $_instance;

    public static function getInstance() {
        if(!self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }


    private function __construct() {

        $host = Config::read('db.host');
        $username = Config::read('db.user');
        $password = Config::read('db.password');
        $database = Config::read('db.basename');

        $this->_connection = new mysqli($host, $username,
            $password, $database);

        if(mysqli_connect_error()) {
            trigger_error("Failed to connect to MySQL: " . mysqli_connect_error(),
                E_USER_ERROR);
        }
    }

    private function __clone() { }


    public function getConnection() {
        return $this->_connection;
    }

    public function __destruct() {
        $this->_connection->close();
    }



}
