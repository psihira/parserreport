<?php


/**
 * Класс по работе с pdf файлами
 *
 * Забирает текст из pdf и находит необходимые значения
 * При нахождении данных записывает их в бд
 *
 *
 */



require_once 'vendor/autoload.php';
require_once('dbCore.php');
require_once('Logs.php');

class parserPDF
{

    private static $_keyWords = [
        1 => ['1.1.1', 'человек'],
        ['1.2.1','человек'],
        ['1.3.1', 'человек'],
        ['1.5', 'баллы'],
        ['1.6', 'баллы'],
        ['1.7','баллы'],
        ['1.8', 'человек'],
        ['1.9', 'человек'],
        ['1.10', 'человек/%'],
        ['1.11', '%'],
        ['1.12', 'человек/%'],
        ['2.2', 'единиц'],
        ['2.3' , 'единиц'],
        ['2.4', 'единиц'],
        ['2.5', 'единиц'],
        ['2.6', 'единиц'],
        ['2.7', 'единиц'],
        ['2.8', 'тыс. руб.'],
        ['2.9', 'тыс. руб.'],
        ['2.10', '%'],
        ['2.11', '%'],
        ['2.12', 'тыс. руб.'],
        ['2.13', 'единиц'],
        ['2.14', '%'],
        ['2.15', 'человек/%'],
        ['2.16', 'человек/%'],
        ['2.17', 'человек/%'],
        ['2.19', 'единиц'],
        ['3. Межд', 'единиц'],
        ['3.1.1', 'человек/%'],
        ['3.2.1', 'человек/%'],
        ['3.4', 'человек/%'],
        ['3.5', 'человек/%'],
        ['3.6', 'человек/%'],
        ['3.7', 'человек'],
        ['3.8', 'человек/%'],
        ['3.9', 'человек/%'],
        ['3.10', 'человек/%'],
        ['3.11', 'тыс. руб.'],
        ['4. Фин', 'тыс. руб.'],
        ['4.2', 'тыс. руб.'],
        ['4.3', 'тыс. руб.'],
        ['4.4', 'тыс. руб.'],
        ['5. Инфр', '%'],
        ['5.1.1', 'кв. м'],
        ['5.3', 'единиц'],
        ['5.4', '%'],
        ['5.5', 'единиц'],
        ['5.6', '%'],
        ['6. Обуч', 'человек/%'],
        ['6.2', 'человек/%'],
        ['6.2.1', 'единиц'],
        ['6.3.1', 'человек'],
        ['6.4.1', 'человек'],
        ['6.5.1', 'человек'],
        ['6.6.1', 'человек'],
        ['6.7.1', 'человек/%']
    ];
    private static $_text='';
    private $_fileName;
    private $_kod;
    private $_vuzID;

    function __construct(){}

    /**
     * @param mixed $fileName
     */
    public function setFileName($fileName)
    {
        $this->_fileName = $fileName;
    }

    /**
     * @param mixed $kod
     */
    public function setKod($kod)
    {
        $this->_kod = $kod;
    }

    /**
     * @param mixed $vuzID
     */
    public function setVuzID($vuzID)
    {
        $this->_vuzID = $vuzID;
    }

    public function startParse()
    {
        self::$_text='';
        $this->_getDataFromFile();

    }

    /**
     * Логир. mysql ошибок
     *
     * @param $error
     */
    private function _mysqlErrorLog($error)
    {
        $errorLog = new Logs();
        $errorLog->createLog($error);
    }

    /**
     * Запись в бд ошибки
     *
     * @param $error
     */
    private function _toLog($error)
    {
        $errorLog = new Logs();
        $errorLog->logReport($this->_vuzID, $this->_kod,$this->_fileName, $error);
    }

    /**
     *Получение текста из pdf
     *
     */
    private function _getDataFromFile()
    {
        $parser = new \Smalot\PdfParser\Parser();
        try{
            $pdf = $parser->parseFile('reports/'.$this->_fileName.'.pdf');
            foreach ($pdf->getPages() as $page) {
                self::$_text .= $page->getText();
            }
            if (self::$_text != ""){
                $this->_analyzeData();
            } else {
                $this->_toLog('pdf error');
            }
        } catch (Exception $e){
            $this->_toLog('pdf error');
        }
    }


    /**
     * Вставка данных в бд
     * @param $data
     */
    private function _insertData($data)
    {
        $db = dbCore::getInstance();
        $connection = $db->getConnection();

        $fields = ['vuz_id','kod','year','file_name','1_1','1_2','1_3',
            '1_4','1_5','1_6','1_7','1_8','1_9_1','1_9_2', '1_10', '1_11_1',
            '1_11_2',  '2_1', '2_2',
            '2_3','2_4','2_5','2_6','2_7', '2_8', '2_9','2_10',
            '2_11', '2_12', '2_13', '2_14_1', '2_14_2', '2_15_1','2_15_2',
            '2_16_1','2_16_2', '2_18', '2_19', '3_1_1',
            '3_1_2','3_2_1', '3_2_2', '3_3_1','3_3_2', '3_4_1', '3_4_2',
            '3_5_1', '3_5_2', '3_6', '3_7_1', '3_7_2', '3_8_1',
            '3_8_2', '3_9_1', '3_9_2', '3_10', '3_11',
            '4_1', '4_2', '4_3','4_4', '5_1', '5_2', '5_3', '5_4', '5_5',
            '5_6_1', '5_6_2',  '6_1_1', '6_1_2',
            '6_2','6_3','6_4', '6_5','6_6', '6_7_1','6_7_2'];

        if (count($data) < count($fields) ){
            while (count($data) < count($fields))
                array_push($data,0);
        }
        $fieldList = '`' . implode('`,`', $fields) . '`';
        $values = "'" . implode("','", array_values($data)) . "'";
        $sql="insert into `mod_sam` ($fieldList) values ($values)";

        try {
            $result = $connection->query($sql);
            if (!$result)  {
                $this->_mysqlErrorLog($connection->errno . " " . $connection->error);
            }
        } catch (mysqli_sql_exception $e) {
            $this->_mysqlErrorLog($e->getMessage());
            exit;
        }

    }


    /**
     * Подробный разбор полученного значения
     *
     * Удаление лишних символов
     * Разбиение на массив
     * Замена символов
     *
     * @param $count
     * @return array|mixed|string
     */
    private function _treatCount($count)
    {
        $count=preg_replace("/[^\d\s\,\/\.]/", '', $count);
        $count = str_replace(',','.',$count);
        $count = ltrim($count,'./ ');
        if (strpos($count, '/') != false) {
            $count = explode("/", $count);
            $count[0]= Trim($count[0]);
            $count[1] = explode(" ", Trim($count[1]))[0];
        } elseif ($count != '') {
            $count = explode(" ", Trim($count))[0];
        } else {
            $count = 0;
        }
        if (count($count) > 9)
            $count = 0;
        return $count;

    }

    /**
     *
     *
     * @param $searchStr
     * @param $point
     * @return bool|string
     */
    private function _searchRightPosition($searchStr, $point)
    {
        if (strpos(self::$_text,str_replace($searchStr,'', $point)) != false){
            $counts = substr(self::$_text, 0 , strpos(self::$_text,str_replace($searchStr,'', $point)));
            return $counts;
        }
        return  false;
    }


    /**
     * Получение значения из текста
     *
     * @param $point
     * @param $word
     * @return array|int|mixed|string
     */
    private function _getCount($point, $word)
    {

        if (!strpos(self::$_text,$word))
            return 0;
        self::$_text = substr(self::$_text,strpos(self::$_text,$word)+strlen($word));
        if (strpos(self::$_text, $point) != false){
            $counts = substr(self::$_text, 0, strpos(self::$_text, $point));
        }elseif (strpos(self::$_text,str_replace(' ','', $point)) !=false){
            $counts = substr(self::$_text, 0 , strpos(self::$_text,str_replace(' ','', $point)));
        }elseif (strpos(self::$_text,str_replace('.','', $point)) !=false){
            $counts = substr(self::$_text, 0 , strpos(self::$_text,str_replace('.','', $point)));
        }elseif(strpos(self::$_text,str_replace('. ','', $point)) !=false){
            $counts = substr(self::$_text, 0 , strpos(self::$_text,str_replace('. ','', $point)));
        }else {
            $counts = self::$_text;
            self::$_text = '';
        }
        return $this->_treatCount(Trim($counts));


    }

    /**
     *Анализ полученного текста из pdf
     *
     *
     */
    private function _analyzeData()
     {
         $resultArr = [];
         array_push($resultArr, $this->_vuzID, $this->_kod, 0, $this->_fileName . '.pdf');

         if ( strpos(self::$_text, '1.1 Общ') != false) {

             $count = 1;
             self::$_text = substr(self::$_text, strpos(self::$_text, '1.1 Общ'));
             foreach (self::$_keyWords as $word) {
                 switch ($count) {
                     case 2:
                         self::$_text = substr(self::$_text, strpos(self::$_text, '1.2 Общ'));
                         break;
                     case 3:
                         self::$_text = substr(self::$_text, strpos(self::$_text, '1.3 Общ'));
                         break;
                     case 4:
                         self::$_text = substr(self::$_text, strpos(self::$_text, '1.4 Ср'));
                         break;
                     case 31:
                         self::$_text = substr(self::$_text, strpos(self::$_text, '3.2 Чис'));
                         break;
                     case 32:
                         self::$_text = substr(self::$_text, strpos(self::$_text, '3.3 Чис'));
                         break;
                     case 46:
                         self::$_text = substr(self::$_text, strpos(self::$_text, '5.2 Кол'));
                         break;
                     case 53:
                         self::$_text = substr(self::$_text, strpos(self::$_text, '6.3 Общ'));
                         break;
                     case 54:
                         self::$_text = substr(self::$_text, strpos(self::$_text, '6.4 Общ'));
                         break;
                     case 55:
                         self::$_text = substr(self::$_text, strpos(self::$_text, '6.5 Общ'));
                         break;
                     case 56:
                         self::$_text = substr(self::$_text, strpos(self::$_text, '6.6 Общ'));
                         break;
                     case 57:
                         self::$_text = substr(self::$_text, strpos(self::$_text, '6.7 Чис'));
                         break;
                 }

                 $result = $this->_getCount($word[0], $word[1]);
                 if (is_array($result)) {
                     array_push($resultArr, $result[0], $result[1]);
                 } else {
                     array_push($resultArr, $result);
                 }
                 $count++;
             }
             $this->_insertData($resultArr);

         } else {
             $this->_toLog('data not found in pdf');
         }

     }
}