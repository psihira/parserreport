<?php

require_once  'parserPDF.php';
require_once 'Logs.php';
require_once 'Links.php';
include 'vendor\autoload.php';

use \Sunra\PhpSimple\HtmlDomParser as HtmlParser;

class parserSite
{
    private $_urlSite;
    private $_kod;
    private $_vuzID;

    /**
     * @param mixed $urlSite
     */
    public function setUrlSite($urlSite)
    {
        $this->_urlSite = trim($urlSite, '/').'/';
    }

    /**
     * @param mixed $kod
     */
    public function setKod($kod)
    {
        $this->_kod = $kod;
    }

    /**
     * @param mixed $vuzID
     */
    public function setVuzID($vuzID)
    {
        $this->_vuzID = $vuzID;
    }

    function __construct(){}

    public function startParseSite()
    {
        if ($this->_getHtml() != '') {
            $this->_findUrl($this->_getHtml());
        } else {
            $this->_toLog('error site');
        }
    }

    private function _toLog($status)
    {
        $errorLog = new Logs();
        $errorLog->logReport($this->_vuzID,$this->_kod,'',$status);
    }

    /**
     * Получение структуры страницы
     *
     * @return \simplehtmldom_1_5\simple_html_dom|string
     */
    private function _getHtml()
    {
        $url = $this->_urlSite . 'sveden/document';
        $ch = curl_init();
        curl_setopt ($ch, CURLOPT_URL, $url);
        curl_setopt ($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux i686; rv:16.0) Gecko/20100101 Firefox/16.0 Iceweasel/16.0');
        $result = curl_exec($ch);
        $content = '';
        if (!curl_errno($ch)){
            $content= HtmlParser::str_get_html($result);
        }
        curl_close($ch);
        return $content;

    }

    /**
     * Загрузка файла по ссылке и его сохранение
     *
     * @param $urlDoc
     * @param $fileName
     * @return int
     */
    private function _downloadFile ($urlDoc, $fileName)
    {
        $path = 'reports/'.$fileName.'.pdf';
        $ch = curl_init();
        $fp = fopen($path, "w+");
        curl_setopt($ch, CURLOPT_URL, $urlDoc);
        curl_setopt($ch, CURLOPT_FAILONERROR, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux i686; rv:16.0) Gecko/20100101 Firefox/16.0 Iceweasel/16.0');
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_exec($ch);
        fclose($fp);
        curl_close($ch);
        if (filesize($path) == 0){
            unlink($path);
            return 0;
        }
        return filesize($path);
    }


    /**
     * Получение конечного pdf файла и передача его на обработку
     *
     * @param $link
     * @param $fileName
     */
    private function _toParser($link, $fileName)
    {
        $links = new Links();
        $parse = new parserPDF();
        $urlDoc_1 = $links->resolveUrl($this->_urlSite. '/sveden/document/' ,$link);
        $urlDoc_2 = $links->resolveUrl($this->_urlSite, $link);
        $size_1 = $this->_downloadFile($urlDoc_1, $fileName);
        $size_2 = $this->_downloadFile($urlDoc_2,'tmpfile');
        if ($size_1 < $size_2){
            copy('reports/tmpfile.pdf','reports/'.$fileName.'.pdf');
        }
        if ($size_1 == $size_2) {
            $this->_toLog('file dont download');
        } else {
            $parse->setFileName($fileName);
            $parse->setKod($this->_kod);
            $parse->setVuzID($this->_vuzID);
            $parse->startParse();
        }

    }

    /**
     * Поиск ссылок по itemprop
     *
     * @param $html
     */
    private function _getByAtt($html)
    {

        $count = 1;
        $fileName = $this->_vuzID;
        foreach ($html->find("[itemprop='ReportEdu_DocLink']") as $elem){
            foreach($elem->find('a') as $link){
                $this->_toParser( $link->href, $fileName);
                $fileName = $this->_vuzID . '-' . $count;
                $count++;
            }
            if (!$elem->find('a')){
                $this->_toParser($elem->href, $fileName);
                $fileName = $this->_vuzID . '-' . $count;
                $count++;
            }

        }
    }

    /**
     * Поиск ссылок по вложенному тексту
     *
     * @param $html
     * @return bool
     */
    private function _getByText($html)
    {
        $count = 1;
        $fileName = $this->_vuzID;
        $result = false;
        foreach ($html->find('text') as $elem) {
            if (strpos($elem, 'самообс') != false) {
                foreach ($elem->find('a') as $link) {
                    $this->_toParser($link->href, $fileName);
                    $fileName = $this->_vuzID . '-' . $count;
                    $count++;
                }
                $result = true;
            }
        }
        return $result;

    }

    /**
     * Передача html для нахождения ссылок
     *
     * @param $html
     */
    private function _findUrl($html)
    {
        if ($html->find("[itemprop='ReportEdu_DocLink']") != null){
            $this->_getByAtt($html);
        }elseif (!$this->_getByText($html)) {
            $this->_toLog('files not found');
        }
    }





}